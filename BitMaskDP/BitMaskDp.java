package tamjid;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.NonNegativeConstraint;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;



public class BitMaskDp {

	static final int INF = 99999;
	
	//int nTestcase; //TODO
	int nParentset;
	int nSubset;
	
	List<Integer> costSubset;
	List<List<Integer>> subsets;
	
	
	List<Integer> subsetBits;
	Result[][] memo;
	
	public List<Integer> getSubsetBit(List<List<Integer>> subsets) {
		List<Integer> subsetBit = new ArrayList<Integer>(subsets.size());
		for(List<Integer> ls: subsets) {
			int bits = 0;
			for(int i: ls) {
				bits |= (1<<i);
			}
			subsetBit.add(bits);
		}
		return subsetBit;
	}
	
	public void input() {
		BufferedReader br = null;
		
		String file = "inputs/input1.txt";

		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			if((line = br.readLine()) != null) {
				nParentset = Integer.parseInt(line);
			}
			if((line = br.readLine()) != null) {
				nSubset = Integer.parseInt(line);
				
			}
			costSubset = new ArrayList<Integer>(nSubset);
			subsets = new ArrayList<List<Integer>>(nSubset);
			
			for(int i=0;i<nSubset;i++) {
				if((line = br.readLine()) != null) {
					
					String[] values = line.trim().split("\\s+");
					
					costSubset.add(Integer.parseInt(values[0]));
					
					int subsetSize = Integer.parseInt(values[1]);
					List<Integer> sub = new ArrayList<>();
					for(int j=0;j<subsetSize;j++) {
						
						sub.add(Integer.parseInt(values[2+j]));
					}
					subsets.add(i,sub);
					
				}
				else {
					System.out.println("\nsubset input missing!");
				}
			}
			
		} catch (IOException e){
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException e) {
			}
		}
	}
	
	public void init() {
		input();
		subsetBits = getSubsetBit(subsets);
		
		memo = new Result[nSubset][1<<nParentset];
		
		//init memo table
		for(int i=0;i<nSubset;i++) {
			for(int j=0;j<(1<<nParentset);j++) {
				memo[i][j] = new Result(-1, 0);
			}
		}
	}
	
	public void printmemo() {
		for(int i=0;i<nSubset;i++) {
			for(int j=0;j<(1<<nParentset);j++) {
				System.out.print(" "+memo[i][j]);
			}
			System.out.println();
		}
	}
	
	public Result setCover(int curIndex, int coveredMask) {
		if(curIndex == nSubset && coveredMask != (1<<nParentset)-1) {
			return new Result(INF,0);
		}
		else if(curIndex == nSubset && coveredMask == (1<<nParentset)-1) {
			return new Result(0,0);
		}
			
		else if(memo[curIndex][coveredMask].cost != -1)
			return memo[curIndex][coveredMask];
		else {
			Result valChoose = setCover(curIndex+1, coveredMask | subsetBits.get(curIndex));
			int valChooseCost =   valChoose.cost + costSubset.get(curIndex) ;
			int valChooseSubset = valChoose.subsets | (1<<curIndex);
			
			Result valNotChoose = setCover(curIndex+1, coveredMask);
			int valNotChooseCost =   valNotChoose.cost ;
			int valueNotChooseSubset = valNotChoose.subsets;
			
			if(valChooseCost <= valNotChooseCost) {
				memo[curIndex][coveredMask].cost = valChooseCost;
				memo[curIndex][coveredMask].subsets = valChooseSubset;
				return memo[curIndex][coveredMask];
			}
			else {
				memo[curIndex][coveredMask].cost = valNotChooseCost;
				memo[curIndex][coveredMask].subsets = valueNotChooseSubset;
				return memo[curIndex][coveredMask];
			}
			
		}
			
	}
	class Result {
		public int cost;
		public int subsets;
		public Result(int cost, int subsets) {
			this.cost = cost;
			this.subsets = subsets;
		}
	}
	
	public int getFvalue() {
		int maxf = -999;
		for(int e=0;e<nParentset;e++) {
			int fcount = 0;
			for(int i=0;i<subsets.size();i++){
				
				for(int li:subsets.get(i)){
					if(li == e){
						fcount++;
						break;
					}
						
				}
			
			}
			if(fcount> maxf) {
				maxf = fcount;
			}
		}
		return maxf;
	}
	
	
	public PointValuePair LPSolve() {
		
		//sum WiXi
		double weights[] = new double[costSubset.size()];
		for(int i=0;i<costSubset.size();i++) {
			weights[i] = (double)costSubset.get(i);
		}
		
		LinearObjectiveFunction objf = new LinearObjectiveFunction(weights, 0);
		
		Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();
		
		//xj>=0
		for(int i=0;i<nSubset;i++) { 
			double[] cons = new double[nSubset];//all are initialized with 0.0
			cons[i] = 1;
			constraints.add(new LinearConstraint(cons, Relationship.GEQ, 0));
			//constraints.add(new LinearConstraint(cons, Relationship.LEQ, 1));
		}
		
		//sum xj>=1 	
		for(int e=0;e<nParentset;e++) {
			//for each element, add one constraint. if e is in 1st and 3rd subset=> x1+x3>=1
			double[] cons = new double[nSubset];
			for(int i=0;i<subsets.size();i++){
				boolean found = false;
				for(int li:subsets.get(i)){
					if(li == e)
						found = true;
				}
				if(found == true)
					cons[i] = 1.0;
			}
			constraints.add(new LinearConstraint(cons, Relationship.GEQ, 1));
		}
		
		SimplexSolver solver = new SimplexSolver();
		PointValuePair optSolution = solver.optimize(new MaxIter(100), objf, 
				new LinearConstraintSet(constraints),
                GoalType.MINIMIZE, new
                NonNegativeConstraint(true));

		return optSolution;
//        double[] solution = optSolution.getPoint();
//        System.out.println("solution:");
//        for(int i=0;i<solution.length;i++) {
//        	System.out.print(" "+solution[i]);
//        }
//        System.out.println("min value:"+optSolution.getValue());
	}
	
	public List<Integer> getLPresult(double[] solution) {
		int f = getFvalue();
		
		List<Integer> LPResult = new ArrayList<Integer>();
		
		for(int i=0;i<solution.length;i++) {
			//System.out.print("-"+solution[i]);
			if(solution[i] >= (double)1/f) {
				LPResult.add(i+1);
				
			}
		}
		return LPResult;
	}
	
	public static void main(String[] args) {
		
		BitMaskDp ob = new BitMaskDp();
		ob.init();
		
		long startTime = System.nanoTime();
		
		Result res = ob.setCover(0, 0);
		
		long endTime   = System.nanoTime();
		
		List<Integer> bitmaskResult = new ArrayList<Integer>();
		for(int i=0;i<ob.nSubset;i++) {
			if(((1<<i)& res.subsets) > 0) {
				bitmaskResult.add(i+1);
			}
		}
		
		System.out.print("BitmaskDP Subset IDs:");
		for(int r:bitmaskResult) {
			System.out.print(" "+r);
		}

		System.out.println();
		System.out.print("BitmaskDP min cost: "+res.cost);
		
		System.out.println();
		
		System.out.println("BitmaskDP runtime: "+(double)(endTime-startTime)/1000000);
		
		System.out.println("--------------------------");
		
		startTime = System.nanoTime();
		
		PointValuePair LPsolution = ob.LPSolve();
		
		endTime   = System.nanoTime();
		
		System.out.print("LP Subset IDs:");
		
		for(int r: ob.getLPresult(LPsolution.getPoint())) {
			System.out.print(" "+r);
		}
		System.out.println();
		System.out.println("LP min cost: "+LPsolution.getValue());
		
		System.out.println("LP runtime: "+(double)(endTime-startTime)/1000000);
		
		

		
	}

}
