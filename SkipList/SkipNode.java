package skiplist;

public class SkipNode {
	
	public static final int NEG_INFINITY = -999999; 
	public static final int POS_INFINITY = 999999; 

	public int key;
	public int value;
	public int index; 
	
	public int dist;

	public SkipNode above, below, before, after;
	
	public SkipNode(int k, int v) {
		key = k;
		value = v;
		above = below = before = after = null;
		if(key == NEG_INFINITY)
			dist = 0;
		else 
			dist = 1;
	}

	public String toString() {
		return "(" + key + "," + value + ")";
	}
}
