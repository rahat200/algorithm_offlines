package skiplist;

import java.util.*;

public class SkipList {
	public SkipNode head;
	public SkipNode tail;

	public int N;//number of elements in skiplist

	public int height;
	public Random rand;

	public SkipList() {
		SkipNode p1, p2;

		p1 = new SkipNode(SkipNode.NEG_INFINITY, 0);
		p2 = new SkipNode(SkipNode.POS_INFINITY, 0);

		head = p1;
		tail = p2;

		p1.after = p2;
		p2.before = p1;

		N = 0;
		height = 0;

		rand = new Random();
	}

	public int size() {
		return N;
	}

	public boolean isEmpty() {
		return (N == 0);
	}



	public SkipNode skipSearch1(int k) {
		SkipNode p = head;

		while (p.below != null) {
			p = p.below;
			while (p.after.key <= k && p.after.key != SkipNode.POS_INFINITY) {
				p = p.after;
			}
		}
		return p;
	}

	public SkipNode skipSearch2(int k) {
		SkipNode p = head;
		while (p.after.key <= k && p.after.key != SkipNode.POS_INFINITY) {
			p = p.after;
			while (p.below != null) {
				p = p.below;
			}
		}

		return p;
	}

//	public Integer get(int k) {
//		SkipNode p;
//
//		//p = findEntry(k);
//		 p = skipSearch1(k);
//
//		if (k == p.key)
//			return (p.value);
//		else
//			return (null);
//	}

	public SkipNode insertAfterAbove(SkipNode p, SkipNode q, int k) {
		SkipNode e;

		e = new SkipNode(k, 0);

		e.before = p;
		e.after = p.after;
		e.below = q;

		p.after.before = e;
		p.after = e;
		q.above = e;

		return (e);
	}

	public void insert(int k, int v) {
		SkipNode p, q;
		int i;

		p = skipSearch1(k);

		if (k == p.key) {
			// same key, just update value
			p.value = v;
			return;
		}

		q = new SkipNode(k, v);
		q.before = p;
		q.after = p.after;
		p.after.before = q;
		p.after = q;

		i = 0;

		while (rand.nextDouble() < 0.5) {

			// System.out.println("before adding new level i:"+i+"h: "+height);
			if (i >= height) {
				// System.out.println("adding new level i:"+i+"h: "+height);
				SkipNode p1, p2;

				height++;

				p1 = new SkipNode(SkipNode.NEG_INFINITY, 0);
				p2 = new SkipNode(SkipNode.POS_INFINITY, 0);

				p1.after = p2;
				p1.below = head;

				p2.before = p1;
				p2.below = tail;

				head.above = p1;
				tail.above = p2;

				head = p1;
				tail = p2;
			}

			while (p.above == null) { // scan backwards

				p = p.before;
			}

			p = p.above;

			q = insertAfterAbove(p, q, k);
			i = i + 1;

		}

		N++;

		// add top empty row
		if (i >= height) {
			//System.out.println("adding new level i:" + i + "h: " + height);
			SkipNode p1, p2;

			height++;

			p1 = new SkipNode(SkipNode.NEG_INFINITY, 0);
			p2 = new SkipNode(SkipNode.POS_INFINITY, 0);

			
			p1.after = p2;
			p1.below = head;

			p2.before = p1;
			p2.below = tail;

			head.above = p1;
			tail.above = p2;

			head = p1;
			tail = p2;
		}

	}

	public void delete(int key) {
		SkipNode p = skipSearch1(key);
		if (p.key != key) {
			System.out.println("key not found!");
			return;
		}

		p.before.after = p.after;
		p.after.before = p.before;

		while (p.above != null) {
			SkipNode prev = p;

			p = p.above;

			p.before.after = p.after;
			p.after.before = p.before;
			p.below = null;
			prev = null;
		}

		// now delete extra levels
		if (head.below.after == null)// only one level in skiplist
			return;

		while (head.below.after.key == SkipNode.POS_INFINITY) {
			System.out.println("deleting head tail");
			
			SkipNode prevhead = head;
			SkipNode prevtail = tail;
			
			head = head.below;
			tail = tail.below;
			
			prevhead = prevtail = null;
			
			if(head.below == null || tail.below == null)
				return;
			
		}

		return;
	}

	public void createIndex () {
		SkipNode p = head;
		while(p.below.below != null)
			p = p.below;
		
		while(p != null) {
			SkipNode start = p;
			while(p.after != null) {
				p = p.after;
				p.dist = getLeftDistance(p);
				if(p.key == SkipNode.NEG_INFINITY)
					System.out.println("-999999");
			}
			p = start.above;
		}
	} 
	
	public int getLeftDistance(SkipNode p) {
		int dist = 0;
		SkipNode left = p.before;
		SkipNode lower = p.below;
		
		while(lower.key != left.key) {
			dist += lower.dist;
			lower = lower.before;
		}
		return dist;
	}
	
	
	public int indexOf(int key) {

		SkipNode p = head;

		while (p.below != null) {
			p = p.below;
		}
		int i = 0;
		while (p.key != key && p.after != null) {
			p = p.after;
			i++;
		}
		return (p.key==key)? i-1 : -1;
	}
	public int indexOf2(int key) {
		
		createIndex();
		
		SkipNode p = head;
		
		while (p.below != null) {
			p = p.below;
		}
		int i = 0;
		while (p.key != key && p.after != null) {
			p = p.after;
			i+=p.dist;
		}
		return (p.key==key)? i-1 : -1;
	}

	public void printSkipList() {
		//String s = "";

		SkipNode p = head;

		while (p.below != null) {
			p = p.below;
		}

		int i = 0;
		while (p != null) {
			p.index = i++;
			p = p.after;
		}

		p = head;

		while (p != null) {
			System.out.println(RowToString(p));

			p = p.below;
		}
		System.out.println();
	}

	public String RowToString(SkipNode p) {
		String s;
		int b, i;
		int a = 0;
		//s = "" + p.key+","+p.dist;
		s = "" + p.key;
		p = p.after;

		while (p != null) {
			SkipNode q;
			q = p;

			while (q.below != null)
				q = q.below;

			b = q.index;
			s = s + " ----";
			for (i = a + 1; i < b; i++)
				s = s + "--------";

			//s = s + "- " + p.key+","+p.dist;
			s = s + "- " + p.key;
			a = b;
			p = p.after;
		}

		return (s);
	}

}
