package SplayTree;

public class SplayTree<T extends Comparable<T>> {

    private SplayNode<T> root;

    public SplayTree() { }

    private void splay(SplayNode<T> node) {
        while (node.hasParent()) {//until get to root
           
        	SplayNode parent = node.getParent();
            
        	if (!parent.hasParent()) { //Zig .. x er parent root
            
        		if (parent.getLeft() == node) {
                
        			rotateRight(parent);
                
        		} else {
                    rotateLeft(parent);
                }
            } else {
                
            	SplayNode gparent = parent.getParent();
                
            	if (parent.getLeft() == node && 
            			gparent.getLeft() == parent) {//ZigZig left
            		
                    rotateRight(gparent);
                    rotateRight(node.getParent());
                
            	} else if (parent.getRight() == node &&
                        gparent.getRight() == parent) {//ZigZig right
            		
                    rotateLeft(gparent);
                    rotateLeft(node.getParent());
                
            	} else if (parent.getLeft() == node &&
                        gparent.getRight() == parent) {//ZigZag right
            		
                    rotateRight(parent);
                    rotateLeft(node.getParent());
                
            	} else {//ZigZag left
            		
                    rotateLeft(parent);
                    rotateRight(node.getParent());
                
            	}
            }
        }
    }

    private void rotateLeft(SplayNode<T> x) {
        
    	SplayNode y = x.getRight();
        
        x.setRight(y.getLeft()); //left child of y is now right child of x
        
        if (y.getLeft() != null) {
            y.getLeft().setParent(x);
        }
        
        y.setParent(x.getParent());
        
        if (x.getParent() == null) {
            root = y;
        
        } else {
        
        	if (x == x.getParent().getLeft()) { //parent er left or right node = y
                x.getParent().setLeft(y);
            } else {
                x.getParent().setRight(y);
            }
        }
        
        y.setLeft(x);
        
        x.setParent(y);
    
    }

    private void rotateRight(SplayNode<T> x) {
        
    	SplayNode y = x.getLeft();
        
    	x.setLeft(y.getRight()); //right child of y is now left child of x
        
    	if (y.getRight() != null) {
            y.getRight().setParent(x);
        }
        
    	y.setParent(x.getParent()); //x er parent ekhn y er parent
        
    	if (x.getParent() == null) {
            root = y;
        
    	} else {
            
    		if (x == x.getParent().getLeft()) {//if x is left child of its parent
                x.getParent().setLeft(y);
            
    		} else {
                x.getParent().setRight(y);
            }
        }
        
    	y.setRight(x);
        x.setParent(y);
    }

    public void insert(T key) {
        if (root == null) {
            root = new SplayNode(key, null);
            return;
        }

        insert(key, root);
        search(key);
    }

    private void insert(T key, SplayNode<T> node) {
        
    	if (key.compareTo( node.getKey() ) < 0) { //left e jabe
        
    		if (node.hasLeft()) { //recursively move to leftmost
                insert(key, node.getLeft());
            
    		} else {
                node.setLeft(new SplayNode(key, node));//create new node at left
            
    		}
        }

        if (key.compareTo(node.getKey())>0) { //right e jabe
            
        	if (node.hasRight()) { //recurisevely right e jabe
                insert(key, node.getRight());
            
        	} else {
                node.setRight(new SplayNode(key, node));//create new one
        	}
        }
    }

    public void delete(T key) {
        if (root == null) {
            return;
        }

        search(key);
        delete(key, root);
    }

    private void delete(T key, SplayNode<T> node) {
    
    	if (key.compareTo(node.getKey())< 0) {
        
    		if (node.hasLeft()) {
                delete(key, node.getLeft());
            }
            
    		return;
        }

        if (key.compareTo(node.getKey()) > 0) {
            
        	if (node.hasRight()) {
                delete(key, node.getRight());
            }
                     
        	return;
        }

        delete(node);
    }

    private void delete(SplayNode<T> node) {
        if (!(node.hasLeft() || node.hasRight())) {
            //System.out.println("case no left right");
            if(node.getParent() == null) {
            	System.out.println("null parent "+node.getKey()+" no left right");
            	root = null;
            	return;
            }
            
            if(node == node.getParent().getLeft()) {
        		node.getParent().setLeft(null);
        	}
        	if(node == node.getParent().getRight()) {
        		node.getParent().setRight(null);
        	}
            
            return;
        }

        if (node.hasLeft() && !node.hasRight()) {//has left child only
           
        	//System.out.println("case only left");
        	//left child ke splice kore current node er position e boshaite hobe	
        	
        	if(node.getParent() == null) {
        		System.out.println("null parent "+node.getKey()+" has left, root"+root.getKey());
        		root = node.getLeft();
        		node.getLeft().setParent(null);
        		return;
        	}
        		
        	if(node == node.getParent().getLeft()) {
        		node.getParent().setLeft(node.getLeft());
        		node.getLeft().setParent(node.getParent());
        	}
        	if(node == node.getParent().getRight()) {
        		node.getParent().setRight(node.getLeft());
        		node.getLeft().setParent(node.getParent());
        	}
            return;
        }

        if (node.hasRight() && !node.hasLeft()) {//has right child only
        	//System.out.println("case only right");
        	//right child ke splice kore current node position e boshaite hobe ekhn
        	
        	if(node.getParent() == null){
        		System.out.println("null parent "+node.getKey()+" has right, root"+root.getKey());
        		root = node.getRight();
        		node.getRight().setParent(null);
        		return;
        	}
        	
        	if(node == node.getParent().getLeft()) {
        		node.getParent().setLeft(node.getRight());
        		node.getRight().setParent(node.getParent());
        	}
        	if(node == node.getParent().getRight()) {
        		node.getParent().setRight(node.getRight());
        		node.getRight().setParent(node.getParent());
        	}
        	
            return;
        }

        // left right 2 child e ase, right er min diye replace korte hobe
        //System.out.println("case both left right");
        SplayNode<T> min = getMinNode(node.getRight());
        node.setKey(min.key);
        
        //System.out.println("min key "+min.getKey());
        
        if(min.hasRight() && min == min.getParent().getRight()) {
        	//System.out.println("has right, on right of parent");
        	min.getParent().setRight(min.getRight());
        	min.getRight().setParent(min.getParent());
        }
        if(min.hasRight() && min == min.getParent().getLeft()) {
        	//System.out.println("has right, on left of parent");
        	min.getParent().setLeft(min.getRight());
        	min.getRight().setParent(min.getParent());
        }
        if(!min.hasLeft() && !min.hasRight()) {
        	//System.out.println("no left right");
        	if(min == min.getParent().getLeft())
        		min.getParent().setLeft(null);
        	if(min == min.getParent().getRight())
        		min.getParent().setRight(null);
        }
//        if(min.hasLeft())
//        	System.out.println("min er left ase, so vuul ase,,");
    }

    private SplayNode<T> getMinNode(SplayNode<T> node) {
        if (!node.hasLeft()) {
            //node.setDeleted(true);
            return node;
        }

        SplayNode<T> min = getMinNode(node.getLeft());
//        if (node.getLeft().isDeleted()) {
//            node.setLeft(null);
//        }
        return min;
    }


    public boolean search(T key) {
        if (root == null) {
            return false;
        }

        SplayNode<T> node = search(key, root);
        splay(node);
        return node != null;
    }

    private SplayNode<T> search(T key, SplayNode<T> node) {
        if (key == node.getKey()) {
            return node;
        }

        if (key.compareTo(node.getKey()) < 0) {
            if (!node.hasLeft()) {
                return null;
            }
            return search(key, node.getLeft());
        }

        if (key.compareTo(node.getKey()) > 0) {
            if (!node.hasRight()) {
                return null;
            }
            return search(key, node.getRight());
        }

        return null;
    }

    public String toString() {
        return root.toString();
    }
    
    public SplayNode<T> getRoot() {
    	return this.root;
    }

}




