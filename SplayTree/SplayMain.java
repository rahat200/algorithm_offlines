package SplayTree;

import java.util.Scanner;

public class SplayMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SplayTree<Integer> st = new SplayTree<Integer>();
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("0-insert\n1=delete\n2=search\n");
		int c;
		 while((c = scanner.nextInt())!= 9) {
			
			switch (c) {
			case 0:
				int k = scanner.nextInt();
				st.insert(k);
				new PrintSplayTree().printNode(st.getRoot());
				break;
			case 1:
				int k1 = scanner.nextInt();
				st.delete(k1);
				new PrintSplayTree().printNode(st.getRoot());
				break;
			case 2:
				int k2 = scanner.nextInt();
				boolean found = st.search(k2);
				if(st.search(k2))
					System.out.println("key"+k2+" found");
				else 
					System.out.println("key"+k2+" NOT found");
				
				new PrintSplayTree().printNode(st.getRoot());
				break;
			case 3:
				
				break;

			default:
				break;
			}
		}
		
//		st.insert(2);
//		st.insert(3);
//		st.insert(4);
//		st.insert(6);
//		st.insert(8);
//		st.insert(5);
//		st.insert(9);
//		st.insert(1);
//		
//		new PrintSplayTree().printNode(st.getRoot());
//		
//		st.delete(8);
//		
//		new PrintSplayTree().printNode(st.getRoot());
//		st.insert(8);
//		
//		new PrintSplayTree().printNode(st.getRoot());
//		
//		
		
		

	}

}
