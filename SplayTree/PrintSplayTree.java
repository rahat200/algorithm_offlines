package SplayTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PrintSplayTree {

    public static <T extends Comparable<T>> void printNode(SplayNode<T> root) {
        int maxLevel = PrintSplayTree.maxLevel(root);

        printNodeInternal(Collections.singletonList(root), 1, maxLevel);
    }

    private static <T extends Comparable<T>> void printNodeInternal(List<SplayNode<T>> nodes, int level, int maxLevel) {
        if (nodes.isEmpty() || PrintSplayTree.isAllElementsNull(nodes))
            return;

        int floor = maxLevel - level;
        int endgeLines = (int) Math.pow(2, (Math.max(floor - 1, 0)));
        int firstSpaces = (int) Math.pow(2, (floor)) - 1;
        int betweenSpaces = (int) Math.pow(2, (floor + 1)) - 1;

        PrintSplayTree.printWhitespaces(firstSpaces);

        List<SplayNode<T>> newNodes = new ArrayList<SplayNode<T>>();
        for (SplayNode<T> node : nodes) {
            if (node != null) {
                System.out.print(node.key);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            PrintSplayTree.printWhitespaces(betweenSpaces);
        }
        System.out.println("");

        for (int i = 1; i <= endgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                PrintSplayTree.printWhitespaces(firstSpaces - i);
                if (nodes.get(j) == null) {
                    PrintSplayTree.printWhitespaces(endgeLines + endgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).left != null)
                    System.out.print("/");
                else
                    PrintSplayTree.printWhitespaces(1);

                PrintSplayTree.printWhitespaces(i + i - 1);

                if (nodes.get(j).right != null)
                    System.out.print("\\");
                else
                    PrintSplayTree.printWhitespaces(1);

                PrintSplayTree.printWhitespaces(endgeLines + endgeLines - i);
            }

            System.out.println("");
        }

        printNodeInternal(newNodes, level + 1, maxLevel);
    }

    private static void printWhitespaces(int count) {
        for (int i = 0; i < count; i++)
            System.out.print(" ");
    }

    private static <T extends Comparable<T>> int maxLevel(SplayNode<T> node) {
        if (node == null)
            return 0;

        return Math.max(PrintSplayTree.maxLevel(node.left), PrintSplayTree.maxLevel(node.right)) + 1;
    }

    private static <T> boolean isAllElementsNull(List<T> list) {
        for (Object object : list) {
            if (object != null)
                return false;
        }

        return true;
    }

}
