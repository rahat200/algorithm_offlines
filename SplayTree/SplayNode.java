package SplayTree;

public class SplayNode<T extends Comparable<T>> {

    
    public SplayNode<T> left;
    public SplayNode<T> right;
    public SplayNode<T> parent;

    public T key;
    private boolean isDeleted = false;

    public SplayNode(T key, SplayNode<T> parent) {
        this.key = key;
        this.parent = parent;
        //System.out.println("key:"+key+"parent: "+parent);
    }


    public boolean hasParent() {
        return parent != null;
    }
    
    public boolean hasLeft() {
        return left != null;
    }

    public boolean hasRight() {
        return right != null;
    }


    public T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    public SplayNode<T> getLeft() {
        return left;
    }

    public void setLeft(SplayNode<T> left) {
        this.left = left;
    }

    public SplayNode<T> getRight() {
        return right;
    }

    public void setRight(SplayNode<T> right) {
        this.right = right;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public SplayNode<T> getParent() {
        return parent;
    }

    public void setParent(SplayNode<T> parent) {
        this.parent = parent;
    }
    
    

}