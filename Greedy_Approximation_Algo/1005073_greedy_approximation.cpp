#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <time.h>

using namespace std;

struct point{
    int x;
    int y;
    point(int x_,int y_): x(x_),y(y_){}
};
struct edge{
    int u;
    int v;
    edge(int u_,int v_): u(u_),v(v_){}
};



int N;

vector<point> points_list;
vector<edge> mst_edges;
vector<int>* mst_adj_list;

vector<int> result_index;

double** cost;

float get_cost(point a, point b) {

    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

void graph_init() {
    //graph input
    cin>>N;//graph size
    int tx,ty;
    for(int i=0; i<N;i++) {
        cin>>tx;
        cin>>ty;
        points_list.push_back(point(tx,ty));
    }

    mst_adj_list = new vector<int>[N];
    //measure cost
    cost = new double*[N];
    for(int i=0; i<N;i++) {

        cost[i] = new double[N];

        for(int j=0; j<N;j++) {
            if(i==j)
                cost[i][j] = INFINITY;
            else{
                cost[i][j] = get_cost(points_list[i],points_list[j]);

            }
        }
    }

    //test

    cout<<"points\n";
    for(int i=0; i<N;i++) {
        cout<<"("<<points_list[i].x<<','<<points_list[i].y<<")"<<'\n';

    }

}

void print_cost_matrix(double** cost) {
    cout<<"\ncost matrix:\n";
    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            cout<<cost[i][j]<<',';
        }
        cout<<'\n';
    }
}

int all_visited(int select[]) {
    for(int i=0; i<N; i++) {
        if(select[i] == 0)
            return 0;
    }
    return 1;
}

void mst_prim() {

    int marked[N][N];
    int select[N];
    int u,v;

    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            marked[i][j] = 0;
        }
    }

    for(int i=0; i<N; i++) {
        select[i] = 0;
    }

    select[0] = 1; //select node 0 first(random)

    while(all_visited(select) != 1) {

        double mini = INFINITY;

        for(int i=0; i<N; i++) {
            if(select[i] == 1) {
                for(int j=0; j<N; j++) {
                    if(select[j]!=1 && marked[i][j] != 1 && cost[i][j] < mini) {
                        mini = cost[i][j];
                        u = i;
                        v = j;
                    }
                }
            }
        }
        mst_edges.push_back(edge(u,v));
        mst_adj_list[u].push_back(v);

        marked[u][v] = 1;
        marked[v][u] = 1;

        select[v] = 1;
    }

}


void preorder(int node) {

    cout<<" "<<node;
    result_index.push_back(node);

    if(mst_adj_list[node].empty())
        return;

    for(int i = 0; i<mst_adj_list[node].size();i++) {
        preorder(mst_adj_list[node][i]);
    }

}





int main() {
    graph_init();

    mst_adj_list = new vector<int>[N];


    clock_t tStart = clock();//start clock


    //print_cost_matrix(cost);
    mst_prim();

    for(int i=0;i<mst_edges.size();i++) {
        cout<<"("<<mst_edges[i].u<<","<<mst_edges[i].v<<") ";
    }

    /*cout<<"adj list";
    for(int i=0;i<N;i++) {
        for(int j = 0; j<mst_adj_list[i].size();j++) {
            cout<<mst_adj_list[i][j]<<" ";
        }
        cout<<"\n";
    }
    */
    cout<<"pre order";
    preorder(0);

    printf("\n\n Time taken: %.5fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

    cout<<"\nresult seq:";
    double path_cost=0;
    for(int i=0; i<result_index.size(); i++){
        cout<<result_index[i];
        cout<<"("<<points_list[result_index[i]].x<<","<<points_list[result_index[i]].y<<"),";
        path_cost += (i==N-1)?cost[result_index[i]][result_index[0]]:cost[result_index[i]][result_index[i+1]];
            //cout<<index[i];
    }
    cout<<"\n\n path cost:"<<path_cost;

    return 0;
}
