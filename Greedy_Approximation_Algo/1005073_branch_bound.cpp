#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <time.h>



using namespace std;

struct point{
    int x;
    int y;
    point(int x_,int y_): x(x_),y(y_){}
};

int N;

vector<point> points_list;
int* result_index;

double** cost;
double* penalty_cost;

float get_cost(point a, point b) {

    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

void calc_cost_matrix() {
    cost = new double*[N];
    for(int i=0; i<N;i++) {

        cost[i] = new double[N];

        for(int j=0; j<N;j++) {
            if(i==j)
                cost[i][j] = INFINITY;
            else{
                cost[i][j] = get_cost(points_list[i],points_list[j]);

            }
        }
    }
}

void graph_init() {
    //graph input
    cin>>N;//graph size
    int tx,ty;
    for(int i=0; i<N;i++) {
        cin>>tx;
        cin>>ty;
        points_list.push_back(point(tx,ty));
    }

    calc_cost_matrix();

    //test

    cout<<"points\n";
    for(int i=0; i<N;i++) {
        cout<<"("<<points_list[i].x<<','<<points_list[i].y<<")"<<'\n';

    }


}

void print_cost_matrix(double** cost) {
    cout<<"\ncost matrix:\n";
    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            cout<<cost[i][j]<<',';
        }
        cout<<'\n';
    }
}



double row_min(double **cost, int i) {
    double min = cost[i][0];
    for(int j=0; j<N; j++) {
        if(cost[i][j] < min) {
            min = cost[i][j];
        }
    }
    return min;
}

double col_min(double **cost, int j) {
    double min = cost[0][j];
    for(int i=0; i<N; i++) {
        if(cost[i][j] < min) {
            min = cost[i][j];
        }
    }
    return min;
}

double row_reduction(double **cost) {
    double vrow,rmin;
    vrow = 0;
    for(int i=0; i<N; i++) {
        rmin = row_min(cost,i);
        if(rmin != INFINITY) {
            vrow += rmin;
        }
        for(int j=0; j<N; j++) {
            if(cost[i][j] != INFINITY){
                cost[i][j] = cost[i][j] - rmin;
            }
        }
    }
    return vrow;
}

double column_reduction(double **cost) {
    double vcol,cmin;
    vcol = 0;
    for(int j=0; j<N; j++) {
        cmin = col_min(cost,j);
        if(cmin != INFINITY) {
            vcol += cmin;
        }
        for(int i=0; i<N; i++) {
            if(cost[i][j] != INFINITY){
                cost[i][j] = cost[i][j] - cmin;
            }
        }
    }
    return vcol;
}

int all_visited(int* select) {
    for(int i=0; i<N; i++) {
        if(select[i] == 0)
            return 0;
    }
    return 1;
}

double check_bounds(int st, int des, double **cost) {
    double** reduced;
    //copy cost to reduced
    //print_cost_matrix(cost);
    reduced = new double* [N];
    for(int i=0; i<N; i++) {
        reduced[i] = new double[N];
        for(int j=0; j<N; j++) {
            reduced[i][j] = cost[i][j];
        }

    }

    //st row to infinity
    for(int j=0; j<N; j++) {

        reduced[st][j] = INFINITY;
    }
    //des col to infinity
    for(int i=0; i<N; i++) {

        reduced[i][des] = INFINITY;
    }

    reduced[des][st] = INFINITY;

    double row = row_reduction(reduced);
    double col = column_reduction(reduced);

    //cout<<"\nrow:"<<st<<"col:"<<des<<"cost:"<<cost[st][des]<<"penalty_cost[st]:"<<penalty_cost[st];
    penalty_cost[des] = penalty_cost[st]+row+col+cost[st][des];
    if(penalty_cost[des] == INFINITY) {
        cout<<"cost infinity.. st:"<<st<<" des:"<<des<<"\n";
        print_cost_matrix(cost);
    }

    return penalty_cost[des];


}


void tsp_branch_bound() {

    int* select = new int[N];
    double* edge_cost = new double[N];

    penalty_cost = new double[N];

    for(int i=0; i<N; i++) {
        select[i] = 0;
    }

    double row = row_reduction(cost);
    double col = column_reduction(cost);
    double t = row+col;
    penalty_cost[0] = t;

    int k = 0; //start node = 0
    int pj = k;

    select[0] = 1;//select first node
    result_index[0] = 0;
    int tmp_count = 1;

    while(all_visited(select) != 1) {

        for(int i=1; i<N; i++) {
            if(select[i] == 0) {
                    //cout<<"k:"<<k<<"i:"<<i<<"\n";
                edge_cost[i] = check_bounds(k,i,cost);
                //cout<<"\nedge cost:"<<edge_cost[i]<<" k:"<<k<<" i:"<<i;
            }
        }
        pj = k;
        double mini = INFINITY;
        for(int i=1; i<N; i++) {
            if(select[i] == 0) {
                if(edge_cost[i] < mini){
                    mini = edge_cost[i];
                    k = i;
                }
            }
        }

        select[k] = 1;
        //save the next selcted node
        //cout<<"\nk selected:"<<k;
        result_index[tmp_count++] = k;

        for(int p=1; p<N; p++) {
            cost[pj][p] = INFINITY;

        }
        for(int p=1; p<N; p++) {
            cost[p][k] = INFINITY;
        }

        cost[k][pj] = INFINITY;

        row_reduction(cost);
        column_reduction(cost);
    }


}

int main(){
    graph_init();
    result_index = new int[N];
    //print_cost_matrix(cost);

    clock_t tStart = clock();//start clock

    tsp_branch_bound();

    printf("\n\n Time taken: %.7fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
    //cost matrix has changed bcz of branch and bound, re-calculate it
    calc_cost_matrix();

    cout<<"\n\nresult seq:";
    double path_cost=0;
    for(int i=0; i<N; i++){
        cout<<result_index[i];
        cout<<"("<<points_list[result_index[i]].x<<","<<points_list[result_index[i]].y<<"),";
        path_cost += (i==N-1)?cost[result_index[i]][result_index[0]]:cost[result_index[i]][result_index[i+1]];
            //cout<<index[i];
    }
    cout<<"\n\n path cost:"<<path_cost;

    return 0;
}
