#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <time.h>



using namespace std;

struct point{
    int x;
    int y;
    point(int x_,int y_): x(x_),y(y_){}
};

int N;

vector<point> points_list;
int* result_index;

double** cost;


float get_cost(point a, point b) {

    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}

void graph_init() {
    //graph input
    cin>>N;//graph size
    int tx,ty;
    for(int i=0; i<N;i++) {
        cin>>tx;
        cin>>ty;
        points_list.push_back(point(tx,ty));
    }

    //measure cost
    cost = new double*[N];
    for(int i=0; i<N;i++) {

        cost[i] = new double[N];

        for(int j=0; j<N;j++) {
            if(i==j)
                cost[i][j] = INFINITY;
            else{
                cost[i][j] = get_cost(points_list[i],points_list[j]);

            }
        }
    }

    //test

    cout<<"points\n";
    for(int i=0; i<N;i++) {
        cout<<"("<<points_list[i].x<<','<<points_list[i].y<<")"<<'\n';

    }


}

void print_cost_matrix(double** cost) {
    cout<<"\ncost matrix:\n";
    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            cout<<cost[i][j]<<',';
        }
        cout<<'\n';
    }
}

void exact_exponential() {
    int* index = new int[N];
    result_index = new int[N];

    for(int i=0; i<N; i++) {
        index[i] = i;
    }
    sort(index,index+N);

    float minCost = INFINITY;


    do {

        float path_cost=0;
        for(int i=0; i<N; i++) {
            path_cost += (i==N-1)?cost[index[i]][index[0]]:cost[index[i]][index[i+1]];
            //cout<<index[i];
        }
        //cout<<"path cost:"<<path_cost<<"\n";

        if(path_cost<minCost) {
            minCost = path_cost;
            for(int i=0; i<N; i++){
                result_index[i] = index[i];
            }

            //cout<<"\n min path cost:"<<path_cost<<"\n";
        }

    }while(next_permutation(index,index+N));

}

int main(){

    graph_init();

    clock_t tStart = clock();//start clock

    exact_exponential();

    printf("\n\n Time taken: %.5fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);


    cout<<"\nresult seq:";
    double path_cost=0;
    for(int i=0; i<N; i++){
        cout<<result_index[i];
        cout<<"("<<points_list[result_index[i]].x<<","<<points_list[result_index[i]].y<<"),";


        path_cost += (i==N-1)?cost[result_index[i]][result_index[0]]:cost[result_index[i]][result_index[i+1]];
        //cout<<"**\n"<<result_index[i];
    }
    cout<<"\n\n path cost:"<<path_cost;

    return 0;
}
