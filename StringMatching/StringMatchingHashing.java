package StringMatching;


public class StringMatchingHashing {

	static int d = 256;
	
	static int q1 = 47;
	static int q2 = 89;
	static int q3 = 101;

	public static void matchPattern(String pattern, String text) {
		int M = pattern.length();
		int N = text.length();
		int i, j;
//		int p = 0; 
//		int t = 0; 
//		int h = 1;
		
		int p1 =0;
		int p2 =0;
		int p3 =0;
		
		int t1 = 0;
		int t2 = 0;
		int t3 = 0;
		
		int h1 = 1;
		int h2 = 1;
		int h3 = 1;

		// (d^M-1)%q
		for (i = 0; i < M - 1; i++){
			//h = (h * d) % q;
			h1 = (h1 * d) % q1;
			h2 = (h2 * d) % q2;
			h3 = (h3 * d) % q3;
		}

		//initial hash value
		for (i = 0; i < M; i++) {
			//p = (d * p + (int) pattern.charAt(i)) % q;
			p1 = (d * p1 + (int) pattern.charAt(i)) % q1;
			p2 = (d * p2 + (int) pattern.charAt(i)) % q2;
			p3 = (d * p3 + (int) pattern.charAt(i)) % q3;
			
			//t = (d * t + (int) text.charAt(i)) % q;
			t1 = (d * t1 + (int) text.charAt(i)) % q1;
			t2 = (d * t2 + (int) text.charAt(i)) % q2;
			t3 = (d * t3 + (int) text.charAt(i)) % q3;
		}

		for (i = 0; i <= N - M; i++) {

			if (p1 == t1 && p2 == t2 && p3 == t3) {
				
				for (j = 0; j < M; j++) {
					if (text.charAt(i + j) != pattern.charAt(j))
						break;
				}

				if (j == M) {//loop didnt break, so match found
					System.out.println("Pattern matched at :" + i);
					break;
				}
			}

			if (i < N - M) {
				//t = (d * (t - text.charAt(i) * h) + text.charAt(i + M)) % q;
				t1 = (d * (t1 - text.charAt(i) * h1) + text.charAt(i + M)) % q1;
				t2 = (d * (t2 - text.charAt(i) * h2) + text.charAt(i + M)) % q2;
				t3 = (d * (t3 - text.charAt(i) * h3) + text.charAt(i + M)) % q3;
				//if t neg, make it pos
//				if (t < 0)
//					t = (t + q);
				if (t1 < 0)
					t1 = (t1 + q1);
				if (t2 < 0)
					t2 = (t2 + q2);
				if (t3 < 0)
					t3 = (t3 + q3);
			}
		}
	}

	public static void main(String[] args) {
		matchPattern("def", "abcdefgdefjj");
	}

}
