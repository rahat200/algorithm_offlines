package StringMatching;

public class StringMatchingNaive {

	static void matchPattern(String pattern, String text) {
		int M = pattern.length();
		int N = text.length();

		for (int i = 0; i <= N - M; i++) {
			int j;

			for (j = 0; j < M; j++)
				if (text.charAt(i + j) != pattern.charAt(j))
					break;

			if (j == M) 
				System.out.println("Pattern matched at  \n" + i);
		}
	}

	public static void main(String[] args) {

		matchPattern("abc", "aceaecabcacea");
	}

}
