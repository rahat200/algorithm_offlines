package StringMatching;

public class StringMatchingKMP {

	static void KMPmatcher(String pattern, String text) {
		int M = pattern.length();
		int N = text.length();

		int j = 0;// pattern index

		int[] prefix_array = getPrefixSuffixArray(pattern);

		int i = 0; // text index
		while (i < N) {
			if (pattern.charAt(j) == text.charAt(i)) {//matching
				j++;
				i++;
			}

			if (j == M) { //full pattern matched
				System.out.println("Found pattern at index :" + (i - j));
				j = prefix_array[j - 1];
			}

			// mismatch after j matches
			else if (i < N && pattern.charAt(j) != text.charAt(i)) {
				if (j != 0)
					j = prefix_array[j - 1];
				else
					i++;
			}
		}

	}

	public static int[] getPrefixSuffixArray(String pattern) {
		int M = pattern.length();
		int length = 0; 
		int i;

		int[] ps_array = new int[M];

		ps_array[0] = 0; //first one always 0
		
		i = 1;
		while (i < M) {
			if (pattern.charAt(i) == pattern.charAt(length)) {//pattern match
				length++;
				ps_array[i] = length;
				i++;
			} 
			else { //mismatch
			
				if (length != 0) {
					length = ps_array[length - 1];
				} else {//len == 0
				
					ps_array[i] = 0;
					i++;
				}
			}
		}

		return ps_array;

	}

	public static void main(String[] args) {
		KMPmatcher("abaccdabc", "ceadbabaccdabcceabce");

	}

}
