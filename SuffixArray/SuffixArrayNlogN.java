package SuffixArray;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Collections;

public class SuffixArrayNlogN {
	
	public static ArrayList<Suffix1> buildSuffixArray(String text) {
		int N = text.length();
		ArrayList<Suffix1> suffixes = new ArrayList<>();
		
		//for length 1 and 2
		for(int i=0;i<N;i++) {
			String s = text.substring(i);
			int rank = (int)text.charAt(i);
			int nextRank = ((i+1)<N) ? (int)text.charAt(i+1) : -1;
			
			Suffix1 sfx = new Suffix1(s,i,rank,nextRank);
			suffixes.add(sfx);
		}
		
		Collections.sort(suffixes);
		
		int [] ind = new int[N];
		
		for (int k = 4; k < 2*N; k = k*2){
	        int rank = 0;
	        int prevRank = suffixes.get(0).rank;
	        
	        suffixes.get(0).rank = 0;
	        ind[suffixes.get(0).index] = 0;

	        //rank
	        for (int i = 1; i < N; i++){
	            if (suffixes.get(i).rank == prevRank &&
	            		suffixes.get(i).nextRank == suffixes.get(i-1).nextRank){
	                prevRank = suffixes.get(i).rank;
	                suffixes.get(i).rank = rank;
	            }
	            else {
	                prevRank = suffixes.get(i).rank;
	                suffixes.get(i).rank = ++rank;
	            }
	            ind[suffixes.get(i).index] = i;
	        }

	        //next rank
	        for (int i = 0; i < N; i++) {
	            int nextindex = suffixes.get(i).index + k/2;
	            suffixes.get(i).nextRank = (nextindex < N)?
	                                  suffixes.get(ind[nextindex]).rank: -1;
	        }

	        Collections.sort(suffixes);
	    }
		
		return suffixes;
	}
	
	public static int getSubstringCount(String text) {
		ArrayList<Suffix1> suffixes = buildSuffixArray(text);
		int ans = suffixes.get(0).suffix.length();
		
		for(int i=0;i<suffixes.size();i++) {
			if(i+1 < suffixes.size()) {
				String s1 = suffixes.get(i).suffix;
				String s2 = suffixes.get(i+1).suffix;
				
				int lcp_length = LCP(s1,s2).length();
				
				ans += s2.length() - lcp_length;
			}
		}
		return ans;
	}
	
	public static String LCP(String a,String b) {
		int i;
		for(i=0;i<a.length()&& i<b.length();i++) {
			if(a.charAt(i) != b.charAt(i))
				break;
		}
		return a.substring(0,i);
	}
	
	public static void main(String[] args) {
		ArrayList<Suffix1> suffixes = buildSuffixArray("bananaa");
		for (Suffix1 s : suffixes) {
			System.out.print(s.index + " ");
		}

		System.out.println(LCP("abcd", "abcdefgh"));
		System.out.println(getSubstringCount("aaaa"));
	}

}

class Suffix1 implements Comparable<Suffix1> {
	String suffix;
	int index;
	
	int rank;
	int nextRank;
	
	public Suffix1(String s, int i,int r1,int r2) {
		this.suffix = s;
		this.index = i;
		
		this.rank = r1;
		this.nextRank = r2;
	}
	
	@Override
	public int compareTo(Suffix1 sfx) {
		if(this.rank == sfx.rank) {
			if(this.nextRank > sfx.nextRank) 
				return 1;
			else 
				return -1;
		}
		else {
			if(this.rank > sfx.rank)
				return 1;
			else 
				return -1;
		}
	}
}
