package SuffixArray;

import java.util.ArrayList;
import java.util.Collections;

public class SuffixArrayNaive {

	public static ArrayList<Suffix> buildSuffixArray(String text) {
		int N = text.length();
		ArrayList<Suffix> suffixes = new ArrayList<>();

		for (int i = 0; i < N; i++) {
			Suffix s = new Suffix(text.substring(i), i);
			suffixes.add(s);
		}

		Collections.sort(suffixes);
		return suffixes;
	}
	
	
	public static int getSubstringCount(String text) {
		ArrayList<Suffix> suffixes = buildSuffixArray(text);
		int ans = suffixes.get(0).suffix.length();
		
		for(int i=0;i<suffixes.size();i++) {
			if(i+1 < suffixes.size()) {
				String s1 = suffixes.get(i).suffix;
				String s2 = suffixes.get(i+1).suffix;
				
				int lcp_length = LCP(s1,s2).length();
				
				ans += s2.length() - lcp_length;
			}
		}
		return ans;
	}
	
	public static String LCP(String a,String b) {
		int i;
		for(i=0;i<a.length()&& i<b.length();i++) {
			if(a.charAt(i) != b.charAt(i))
				break;
		}
		return a.substring(0,i);
	}
	

	public static void main(String[] args) {
		ArrayList<Suffix> suffixes = buildSuffixArray("banana");
		System.out.print("suffix array:");
		for (Suffix s : suffixes) {
			System.out.print(s.index + " ");
		}
		//System.out.println(LCP("abcd", "abcdefgh"));
		System.out.println();
		System.out.println("number of distinct substring: ");
		System.out.print(getSubstringCount("aaaa"));

	}
}

class Suffix implements Comparable<Suffix> {
	String suffix;
	int index;

	public Suffix(String s, int i) {
		this.suffix = s;
		this.index = i;
	}

	@Override
	public int compareTo(Suffix sfx) {
		if (this.suffix.compareTo(sfx.suffix) > 0)
			return 1;
		else
			return -1;
	}
}
